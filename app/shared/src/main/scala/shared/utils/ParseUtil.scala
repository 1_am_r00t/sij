package shared.utils

import scala.util.control.Exception.allCatch

/**
  * Helps with parsing strings.
  * <p>
  * Created by Matthias Braun on 12/19/2016.
  */
object ParseUtil {
  /**
    * Tries to convert the `stringToParse` to an integer. Returns None if that fails.
    */
  def parseInt(stringToParse: String): Option[Int] = allCatch.opt(stringToParse.toInt)

}
