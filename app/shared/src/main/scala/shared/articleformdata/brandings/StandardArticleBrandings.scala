package shared.articleformdata.brandings

import shared.articleformdata.ArticleFormIds.BrandingIds
import shared.pages.HtmlId


/**
  * Utility functions for dealing with standard (print, etch, stitch, engrave) [[shared.articleformdata.ArticleBranding]]s
  * <p>
  * Created by Matthias Braun on 10/25/2016.
  */
object StandardArticleBrandings {

  case class StandardArticleBranding(id: HtmlId, name: String)

  val print1c = StandardArticleBranding(BrandingIds.print1c, "1c print")
  val printAdditionalColor = StandardArticleBranding(BrandingIds.printAdditionalColor, "additional color print")
  val print2c = StandardArticleBranding(BrandingIds.print2c, "2c print")
  val print3c = StandardArticleBranding(BrandingIds.print3c, "3c print")
  val print4c = StandardArticleBranding(BrandingIds.print4c, "4c print")
  val digitalPrint4c = StandardArticleBranding(BrandingIds.digitalPrint4c, "4c digital print")
  val engraving = StandardArticleBranding(BrandingIds.engraving, "engraving")
  val etching = StandardArticleBranding(BrandingIds.etching, "etching")
  val stitching = StandardArticleBranding(BrandingIds.stitching, "stitching")

  val allBrandings = Seq(
    print1c,
    printAdditionalColor,
    print2c,
    print3c,
    print4c,
    digitalPrint4c,
    engraving,
    etching,
    stitching
  )

  private val idsAndNames = allBrandings.map(branding => branding.id -> branding.name).toMap
  private val namesAndIds = allBrandings.map(branding => branding.name -> branding.id).toMap

  def nameFor(brandingId: HtmlId): Option[String] = idsAndNames get brandingId

  def idFor(brandingName: String): Option[HtmlId] = namesAndIds get brandingName

  val names = allBrandings map (_.name)
  val ids = allBrandings map (_.id)
}
