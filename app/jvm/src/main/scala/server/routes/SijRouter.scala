package server.routes

import java.nio.file.{Path, Paths}

import akka.actor.ActorRefFactory
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, HttpResponse}
import akka.http.scaladsl.server.{Directives, Route, StandardRoute}
import akka.stream.Materializer
import server.Server
import server.pages.SkeletonPage
import server.persistency.Articles
import server.strings.ServerLogStrings.ServerStringImplicits.serverString2String
import server.strings.{ServerLogStrings, ServerStringsForClient}
import shared.articleformdata._
import shared.articles.ArticleOrderings
import shared.merchantsandsuppliers.MerchantsAndSuppliers
import shared.pages.HiddenFields.HiddenFieldKeys
import shared.pages.{PageId, PageInfo}
import shared.serverresponses.ServerStringForClient
import shared.{SijAjaxApi, SijRestApi}
import slogging.LazyLogging
import upickle.default.{Reader, Writer}
import serverutils.{CsvUtil, IO, JsonUtil, Tasks}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success}
import scalaz.{-\/, \/-}

/**
  * Defines what happens when we send requests to the server.
  * <p>
  * Created by Matthias Braun on 12/10/2016.
  */
object SijRouter extends Directives with LazyLogging {

  private def logHeadersAndIp(innerRoute: Route): Route = {
    /* To get the IP address of the requesting machine, we need to have this piece of configuration in resources/application.conf:
    server {
    remote-address-header = on
    ...
     */
    extract(_.request.headers) { headers =>
      val headersString = headers.map(h => s"${h.name}:${h.value}").mkString(", ")
      logger.info(s"Headers: $headersString")
      innerRoute
    }
  }

  private def skeletonPage(title: String, id: PageId, fields: Map[String, Any] = Map()) =
    complete {
      HttpEntity(ContentTypes.`text/html(UTF-8)`, new SkeletonPage(title, id, fields).content.render)
    }

  private def newArticlePage(id: MerchantOrSupplierId) = {
    val infoAboutMerchantOrSupplier = Map(HiddenFieldKeys.MerchantOrSupplierId -> id.id)
    skeletonPage(PageInfo.NewArticle.title, PageInfo.NewArticle.id, infoAboutMerchantOrSupplier)
  }

  private def articleOverviewPage(id: MerchantOrSupplierId) = {
    val infoAboutMerchantOrSupplier = Map(HiddenFieldKeys.MerchantOrSupplierId -> id.id)
    skeletonPage(PageInfo.ArticleOverview.title, PageInfo.ArticleOverview.id, infoAboutMerchantOrSupplier)
  }


  private def toCsvFile(articlesWithMetaInfo: Seq[ArticleWithMetaInfo]): Either[Exception, Path] = {
    val articles = articlesWithMetaInfo.map(_.articleFormData)
    val csv = CsvUtil.toCsv(articles)
    IO.write(Paths.get("/tmp/articles.csv"), csv)
  }


  private def htmlUtf8Entity(response: ServerStringForClient) =
    HttpEntity(ContentTypes.`text/html(UTF-8)`, response.value)
  private def sendToClient(response: ServerStringForClient): StandardRoute =
    complete(htmlUtf8Entity(response))

  private def sendToClient(futureResponse: scala.concurrent.Future[ServerStringForClient]): StandardRoute =
    complete(
      futureResponse.map(response => HttpResponse(entity = htmlUtf8Entity(response)))
    )

  private def sendToClient(futureResponse: scalaz.concurrent.Future[ServerStringForClient]): StandardRoute =
    sendToClient(Tasks.toScalaFuture(futureResponse))
  /**
    * Maps paths in the URL to actions.
    *
    * @param actorRefFactory needed to load the JavaScript.
    * @return a route for this application
    */
  def route(implicit actorRefFactory: ActorRefFactory, materializer: Materializer): Route = {


    def sortAndToClientString(articles: Seq[ArticleWithMetaInfo]): ServerStringForClient = {
      // In the catalog, the articles are sorted by category and by their position within that category
      val sortedArticles = articles.sorted(ArticleOrderings.byCategoryAndPosition)
      // Convert the articles to JSON and make clear that the JSON is meant for the client
      ServerStringForClient(JsonUtil.toJson(sortedArticles))
    }

    get {
      pathSingleSlash(
        articleOverviewPage(MerchantsAndSuppliers.unknownMerchant.id)
      ) ~ // Otherwise the JavaScript code is not loaded
        getFromResourceDirectory("")
    } ~
      (path(PageInfo.ChooseCompany.path) & get) {
        skeletonPage(PageInfo.ChooseCompany.title, PageInfo.ChooseCompany.id)
      } ~
      get {
        path(Segment / PageInfo.NewArticle.path) { merchantOrSupplierId =>
          newArticlePage(MerchantOrSupplierId(merchantOrSupplierId))
        }
      } ~
      get {
        path(Segment / PageInfo.ArticleOverview.path) { merchantOrSupplierId =>
          articleOverviewPage(MerchantOrSupplierId(merchantOrSupplierId))
        }
      } ~
      (path(PageInfo.ChooseCompany.path) & get) {
        skeletonPage(PageInfo.ChooseCompany.title, PageInfo.ChooseCompany.id)
      } ~
      (path(PageInfo.AllArticles.path) & get) {
        skeletonPage(PageInfo.AllArticles.title, PageInfo.AllArticles.id)
      } ~
      (path(SijRestApi.Paths.EditArticle) & get) {
        parameters(SijRestApi.Parameters.ArticleId.as[String])
          .as(ArticleId) { id =>
            val infoAboutArticleToEdit = Map(HiddenFieldKeys.ArticleId -> id)
            skeletonPage(PageInfo.EditArticle.title, PageInfo.EditArticle.id, infoAboutArticleToEdit)
          }
      } ~
      (path(SijRestApi.Paths.GetArticles) & get) {
        // If the request includes an ID for a merchant or supplier, we respond with the articles of that merchant or supplier.
        // If there's no ID in the request, we return the articles of all merchants and suppliers.
        parameters(SijRestApi.Parameters.MerchantOrSupplierId.as[String] ?) { idMaybe =>
          val idsOfMerchantsOrSuppliers = idMaybe.fold(MerchantsAndSuppliers.idsOfAll)(id => Seq(MerchantOrSupplierId(id)))
          logger.info(ServerLogStrings.requestForArticlesOf(idsOfMerchantsOrSuppliers))
          logHeadersAndIp {
            // Retrieve the articles from the database and send them as JSON to whoever requested it.
            val futureResponse = Articles.latestArticlesOf(idsOfMerchantsOrSuppliers).map {
              case \/-(articles) => sortAndToClientString(articles)
              case -\/(e) =>
                logger.warn(ServerLogStrings.couldNotGetArticlesOf(idsOfMerchantsOrSuppliers, e))
                ServerStringsForClient.errorInDb(e)
            }
            sendToClient(futureResponse)
          }
        }
      } ~
      (path(SijRestApi.Paths.GetArticlesOfAllSuppliers) & get) {
        logger.info(ServerLogStrings.requestForAllSupplierArticles)
        val allSuppliers = MerchantsAndSuppliers.suppliers.map(_.id)
        logHeadersAndIp {
          val response = Articles.latestArticlesOf(allSuppliers).map {
            case \/-(articles) => sortAndToClientString(articles)
            case -\/(e) =>
              logger.warn(ServerLogStrings.couldNotGetSupplierArticles(e))
              ServerStringsForClient.errorInDb(e)
          }
          sendToClient(response)
        }
      } ~
      (path(SijRestApi.Paths.GetArticlesAsCsv) & get) {
        logger.info(ServerLogStrings.requestForArticlesAsCsv)

        val futureRoute = Articles.latestArticlesOf(MerchantsAndSuppliers.idsOfAll).map {
          case \/-(articles) =>
            toCsvFile(articles) match {
              case Right(pathToCsvFile) =>
                val csvFile = pathToCsvFile.toFile
                logger.info(s"Successfully wrote to ${csvFile.getName}")
                getFromFile(csvFile, ContentTypes.`text/csv(UTF-8)`)

              case Left(exception) =>
                logger.warn(s"Could not write articles as CSV to file: $exception")
                sendToClient(ServerStringsForClient.couldNotGetArticlesAsCsv(exception))
            }
          case -\/(e) =>
            logger.warn(ServerLogStrings.couldNotGetAllArticles(e))
            sendToClient(ServerStringsForClient.errorInDb(e))
        }

        logHeadersAndIp {
          onComplete(Tasks.toScalaFuture(futureRoute)) {
            case Success(succeededRoute) => succeededRoute
            case Failure(error) => sendToClient(ServerStringsForClient.couldNotGetArticlesAsCsv(error))
          }
        }
      } ~
      post {
        path(SijRestApi.AjaxPath / Segments) { segments =>
          logHeadersAndIp {
            decodeRequest {
              entity(as[String]) { requestAsString =>
                complete(handleRequest(segments, requestAsString))
              }
            }
          }
        }
      }
  }

  /**
    * Used for AJAX calls from the client.
    */
  private object AjaxRouter extends autowire.Server[String, Reader, Writer] {

    // There must be a Reader[Result] implicitly available
    def read[Result: Reader](string: String): Result = upickle.default.read[Result](string)

    // There must be a Writer[Result] implicitly available
    def write[Result: Writer](result: Result): String = upickle.default.write(result)
  }

  /**
    * Handles a request sent from the client.
    *
    * @param segments
    *                contains the package, the class and the name of the API method to call
    * @param request the request from the client
    * @return a future answer to the request
    */
  private def handleRequest(segments: List[String], request: String): Future[String] =
  // We get the API method from the segments and call it with the request as the parameter
    AjaxRouter.route[SijAjaxApi](Server)(autowire.Core.Request(segments, upickle.default.read[Map[String, String]](request)))
}
