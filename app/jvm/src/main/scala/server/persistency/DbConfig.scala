package server.persistency

import server.persistency.couchdb.DbConnectionInfo
import slogging.LazyLogging

/**
  * Provides database configuration.
  * <p>
  * Created by Matthias Braun on 11/5/2016.
  */
object DbConfig extends LazyLogging {

  //private val couchDbHttpsPort = 6984
  // Must be the same as in local.ini and docker-compose.yml
  private val couchDbHttpPort = 50392
  // You can set these environment variables in the Sij section of docker-compose.yml
  private val couchDbPassword = sys.env.getOrElse("COUCHDB_PASSWORD", "You have to set a CouchDB password via the environment variable")
  private val couchDbUsername = sys.env.getOrElse("COUCHDB_USER", "You have to set a CouchDB username via the environment variable")

  // Must be the same as in docker-compose.yml
  private val couchDbHostName = "couchDbForSij"
  val connectionInfo = DbConnectionInfo(hostName = couchDbHostName, databaseName = "sij-database", port = couchDbHttpPort,
    shouldUseHttps = false, username = couchDbUsername, password = couchDbPassword)
}
