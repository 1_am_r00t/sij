package server.persistency.couchdb

import com.ibm.couchdb.{CouchDbApi, CouchDocs, CouchKeyVals, TypeMapping}
import shared.articleformdata._
import slogging.LazyLogging

import scalaz.concurrent.Task

/**
  * Lets us query our CouchDB.
  * <p>
  * Created by Matthias Braun on 10/24/2016.
  */
object CouchDbQueries extends LazyLogging {


  // This is the type name of an article in the CouchDB
  val articleDocKind = "Article"
  // Define a type mapping used to transform class names into the doc kind
  val typeMapping = TypeMapping(classOf[ArticleWithMetaInfo] -> articleDocKind)


  def getAllArticles(dbApi: CouchDbApi): Task[CouchDocs[(String, String), String, ArticleWithMetaInfo]] =
  // Retrieve all articles from the database and deserialize them to the right type.
  // byType[String] refers to the type of the "kind" variable in the database. "kind" is another word for type
    dbApi.docs.getMany.includeDocs[ArticleWithMetaInfo].byType[String](
      ViewsAndDesigns.Views.Names.filterByKind, ViewsAndDesigns.Designs.Names.filterByKind,
      typeMapping.get(classOf[ArticleWithMetaInfo]).get).build.query


  def getReducedViewOfAllArticles(dbApi: CouchDbApi) = {
    import ViewsAndDesigns._
    // The type parameters are for the key and the value the query returns. The key is the article ID
    val reducedArticleView = dbApi.query.view[
      String, (String, Int, String, MerchantOrSupplier, String, CatalogData, String)
      ](Designs.Names.partsOfReducedArticle, Views.Names.partsOfReducedArticle).get

    reducedArticleView.build.query
  }

  def filterByArticleId(id: ArticleId, db: CouchDbApi): Task[CouchDocs[String, ArticleWithMetaInfo, ArticleWithMetaInfo]] = {

    val articleIdView = createArticleIdView(db)
    articleIdView.key(id.id).includeDocs[ArticleWithMetaInfo].build.query
  }

  private def createArticleIdView(dbApi: CouchDbApi) = {
    import ViewsAndDesigns._

    // The type parameters are for the key and the value the query returns. The key is the article ID
    dbApi.query.view[String, ArticleWithMetaInfo](Designs.Names.filterByArticleId,
      Views.Names.filterByArticleId).get
  }

  def filterByArticleIds(ids: Seq[ArticleId], dbApi: CouchDbApi) = {

    val articleIdView = createArticleIdView(dbApi)

    articleIdView.withIds(ids.map(_.id)).build.query
  }


  def filterByMerchantOrSupplier(id: MerchantOrSupplierId, db: CouchDbApi): Task[CouchKeyVals[String, ArticleWithMetaInfo]] = {

    import ViewsAndDesigns._

    // The type parameters are for the key and the value the query returns. The key is the merchant or supplier ID
    val nameView = db.query.view[String, ArticleWithMetaInfo](Designs.Names.filterByMerchantOrSupplierId,
      Views.Names.filterByMerchantOrSupplierId).get

    nameView.key(id.id).build.query

  }

}
