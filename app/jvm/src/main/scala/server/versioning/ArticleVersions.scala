package server.versioning

import shared.articleformdata.ArticleWithMetaInfo

/**
  * Helps with versioning articles.
  * <p>
  * Created by Matthias Braun on 11/15/2016.
  */
object ArticleVersions {
  def setCreationDate(articleWithMetaInfo: ArticleWithMetaInfo, newCreationDate: Long): ArticleWithMetaInfo = {

    val oldMetaInfo = articleWithMetaInfo.metaInfo
    val newMetaInfo = oldMetaInfo.copy(oldMetaInfo.articleId, oldMetaInfo.versionCount, newCreationDate)

    articleWithMetaInfo.copy(articleWithMetaInfo.articleFormData, newMetaInfo)
  }

  def incrementVersion(articleWithMetaInfo: ArticleWithMetaInfo): ArticleWithMetaInfo = {
    val oldMetaInfo = articleWithMetaInfo.metaInfo
    val oldVersion = oldMetaInfo.versionCount

    val newVersion = oldVersion.increment

    val newMetaInfo = oldMetaInfo.copy(oldMetaInfo.articleId, newVersion, oldMetaInfo.creationDate)

    articleWithMetaInfo.copy(articleWithMetaInfo.articleFormData, newMetaInfo)
  }

}
