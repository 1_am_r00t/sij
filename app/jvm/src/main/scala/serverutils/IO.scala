package serverutils

import java.nio.charset.StandardCharsets
import java.nio.file.{Files, Path, StandardOpenOption}
import java.util.Locale

import scala.io.Source

/**
  * Helps with reading and writing files.
  * <p>
  * Created by Matthias Braun on 10/23/2016.
  */
object IO {

  private val utf8 = StandardCharsets.UTF_8

  def read(pathToFile: Path): String = {
    val sourcedFile = Source.fromFile(pathToFile.toFile, utf8.displayName(Locale.ROOT))
    val fileContent = sourcedFile.mkString
    sourcedFile.close()
    fileContent
  }

  def write(destinationFile: Path, fileContent: String): Either[Exception, Path] =
    write(destinationFile, fileContent.getBytes(utf8))

  def write(destinationFile: Path, fileContent: Array[Byte]): Either[Exception, Path] =
    try {
      Files.createDirectories(destinationFile.getParent)
      // Return the path to the destinationFile if successful
      Right(Files.write(destinationFile, fileContent))
    } catch {
      case exception: Exception => Left(exception)
    }

}
