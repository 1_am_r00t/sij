package client.pages.allarticles.table

import shared.pages.HtmlId

/**
  * Provides HTML IDs of the fields inside the table that shows all articles of all merchants and suppliers.
  * <p>
  * Created by Matthias Braun on 12/12/2016.
  */
object ArticleTableIds {


  val articleName = HtmlId("article-name")
  val articleDescription = HtmlId("article-description")
  val articleNrInCatalog = HtmlId("article-nr-in-catalog")
  val positionInCatalog = HtmlId("position-in-catalog")
  val pageInCatalogOfArticle = HtmlId("page-in-catalog-of-article")
  val remarksForCatalogCreation = HtmlId("remarks-for-catalog-creation")
  val articleNrByMerchantOrSupplier = HtmlId("article-nr-assigned-by-merchant-or-supplier")
}
