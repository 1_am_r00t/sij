package client.utils.gui.inputs

import org.scalajs.dom.html.{Div, Element}
import shared.pages.{GuiString, HtmlId}

import scalacss.ScalatagsCss._
import scalacss.internal.StyleA
import scalatags.JsDom.TypedTag
import scalatags.JsDom.all._

/**
  * With HTML file inputs, the user can pick files from their local hard drive.
  * <p>
  * Created by Matthias Braun on 12/16/2016.
  */
object FileInputs {

  def withLabel(divClass: StyleA, inputId: HtmlId, labelText: GuiString, labelIcon: TypedTag[Element],
                         labelClass: StyleA): TypedTag[Div] =
    div(divClass)(
      label(`for` := inputId.value, `class` := labelClass.htmlClass)(labelText.value, labelIcon),
      input(id := inputId.value, `type` := "file")
    )
}
