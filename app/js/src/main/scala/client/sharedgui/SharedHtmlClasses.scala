package client.sharedgui

import scalacss.Defaults._
import scalatags.JsDom.all._

/**
  * HTML classes shared among different pages of this application.
  * <p>
  * Created by Matthias Braun on 10/24/2016.
  */
class SharedHtmlClasses extends StyleSheet.Inline {

  import dsl._

  val pureButton: StyleA = fromClassName("pure-button")
  val primaryButton: StyleA = fromClassName("pure-button-primary")
  val largeButton: StyleA = fromClassName("button-large")

  def fromClassName(className: String): StyleA = style(addClassName(className))

  // The "_*" type annotation tells the compiler that we want to use the input as varargs and not as a Seq
  def fromClassNames(classNames: String*): StyleA = style(addClassNames(classNames: _*))

  /**
    * Converts a [[StyleA]] to a `class` [[Modifier]]. This is useful when we want to pass
    * one of our styles as a [[Modifier]] to an HTML tag using ScalaTags.
    *
    * @param style the [[StyleA]] we want to convert to a `class` [[Modifier]]
    * @return the [[StyleA]] as a `class` [[Modifier]]
    */
  def toModifier(style: StyleA): Modifier = `class` := style.htmlClass
}

object SharedHtmlClasses extends SharedHtmlClasses {

  val longTextInputInRows: StyleA = fromClassName("long-text-input-in-rows")
  val bigTextAreaInRows : StyleA = fromClassName("big-text-area-in-rows")

  val shortInput: StyleA = fromClassName("pure-u-1-2")

  val screenReaderOnly: StyleA = fromClassName("sr-only")

  val deadlineOver: StyleA = fromClassName("deadline-over")

  val verticalMiddle: StyleA = fromClassName("vertical-middle")
  val horizontalSpace: StyleA = fromClassName("horizontal-space")

  val strongSeparator: StyleA = fromClassName("strong")
  val subtleSeparator: StyleA = fromClassName("subtle")

  val header: StyleA = fromClassName("header")

  val hidden: StyleA = fromClassName("hidden")

  val controlGroup: StyleA = fromClassName("pure-control-group")

  /**
    * We use this class to make input fields longer so the user can enter more characters before the text is cut off in
    * text inputs or a line break occurs in text areas.
    */
  val bigTextAreas: StyleA = fromClassName("pure-input-1-4")
  val longTextInputs: StyleA = fromClassName("pure-input-1-4")
  val veryLongInput: StyleA = fromClassName("pure-input-1-2")


}
