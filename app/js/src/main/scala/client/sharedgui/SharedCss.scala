package client.sharedgui

import org.scalajs.dom.html.Document
import shared.pages.HtmlId

import scalacss.Defaults._
import scalacss.internal.ValueT.Color
import scalacss.internal.{FontFace, Length, ValueT}

/**
  * CSS shared among different pages of this application.
  * <p>
  * Created by Matthias Braun on 10/24/2016.
  */
class SharedCss extends StyleSheet.Standalone {

  import dsl._

  forStyle(SharedHtmlClasses.hidden) - display.none

  forStyle(SharedHtmlClasses.verticalMiddle) - verticalAlign.middle

  /**
    * Default values for all horizontal separators.
    */
  object hrDefaults {
    val borderThickness = 1 px
    val margin = 15 px
  }

  forStyle(SharedHtmlClasses.longTextInputInRows) - width(300 px)
  forStyle(SharedHtmlClasses.bigTextAreaInRows) - (width(300 px), height(100 px))

  forStyle(SharedHtmlClasses.deadlineOver) - (
    color.orangered,
    fontSize(130 %%)
  )

  forStyle(SharedHtmlClasses.subtleSeparator) - (
    border(0 px),
    height(1 px),
    backgroundImage := "linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.15), rgba(0, 0, 0, 0))"
  )

  forStyle(SharedHtmlClasses.strongSeparator) - (
    border(0 px),
    height(1 px),
    backgroundImage := "linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.35), rgba(0, 0, 0, 0))",

    marginTop(2 em),
    marginBottom(2 em)
  )

  forStyle(SharedHtmlClasses.header) - (

    fontFamily(Fonts.raleway),
    textAlign.center,
    color.rgb(61, 61, 61),
    fontSize(130 %%)
  )

  forStyle(SharedHtmlClasses.horizontalSpace) - (
    display.inlineBlock,
    width(1 em)

  )

  /**
    * Provides fonts for the pages of this application.
    * <p>
    * Created by Matthias Braun on 11/7/2016.
    */

  object Fonts extends StyleSheet.Inline {
    val raleway: FontFace = fontFace("Raleway")(
      _.src("url(http://fonts.gstatic.com/s/raleway/v11/8KhZd3VQBtXTAznvKjw-k_k_vArhqVIZ0nv9q090hN8.woff2)")
        .fontWeight._200
        .fontStyle.normal
    )
  }

  val defaultBackgroundColor = backgroundColor.ivory

  def getTextShadow(xOffset: Length[Int],
                    yOffset: Length[Int],
                    blurRadius: Length[Int],
                    shadowColor: ValueT[Color]) = {

    val xOffsetStr = toString(xOffset)
    val yOffsetStr = toString(yOffset)
    val blurRadiusStr = toString(blurRadius)
    val shadowColorStr = shadowColor.value.toString

    s"$xOffsetStr $yOffsetStr $blurRadiusStr $shadowColorStr"
  }

  def toString(length: Length[_]): String = {
    // For example: "10px"
    s"${length.n}${length.u}"
  }

  /**
    * Adds this CSS to the head of the `document`.
    *
    * @param document we add the CSS to this [[Document]]
    * @return the added CSS as a [[org.scalajs.dom.raw.Node]]
    */
  def addTo(document: Document) = {
    val css = document.createElement("style")
    css.innerHTML = this.render
    css.setAttribute("type", "text/css")
    document.head.appendChild(css)
  }

  /**
    * Creates a CSS selector for the HTML elements of the given `class`.
    *
    * @param htmlClass the HTML element for which we create the CSS selector are identified by this class
    * @return a selector for the class of HTML elements
    */
  protected def forClass(htmlClass: String) = s".$htmlClass"

  protected def forStyle(style: StyleA) = s".${style.htmlClass}"

  /**
    * Creates a CSS selector for an HTML element.
    *
    * @param id the HTML element is identified by this [[shared.pages.HtmlId]]
    * @return a selector for the HTML element
    */
  protected def forElement(id: HtmlId) = s"#$id"
}
